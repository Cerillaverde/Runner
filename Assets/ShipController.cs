using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipController : MonoBehaviour
{
    int direccionX;
    int direccionY;
    Vector2 velocidad;
    public GameObject shoot;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Movement());
        StartCoroutine(Shoot());
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    IEnumerator Shoot()
    {
        while (true)
        {
            GameObject shooting = Instantiate(shoot);
            shooting.transform.position = this.transform.position;
            if (Singleton.Instance.level >= 20)
                yield return new WaitForSeconds(1);
            else
                yield return new WaitForSeconds(10 - (Singleton.Instance.level/2));
        }
    }

    IEnumerator Movement()
    {
        while (true) {
            if (this.transform.position.x <= -6)
            {
                direccionX = 1;
            }else if (this.transform.position.x >= 9)
            {
                direccionX = -1;
            }
            else
            {
                direccionX = Random.Range(0, 2) == 0 ? -1 : 1;
            }

            if (this.transform.position.y >= 5)
            {
                direccionY = -1;
            }else if (this.transform.position.y <= 3.8)
            {
                direccionY = 1;
            }
            else
            {
                direccionY = Random.Range(0, 2) == 0 ? -1 : 1;
            }

            velocidad = new Vector2(Random.Range(0, 3) * direccionX, Random.Range(0, 2) * direccionY);
            this.GetComponent<Rigidbody2D>().velocity = velocidad;
            yield return new WaitForSeconds(1f);
        }
    }
}
