using System.Collections;
using UnityEngine;

public class spawnerDown : MonoBehaviour
{
    public GameObject[] enemy;
    Vector2 spawnPos;
    // Start is called before the first frame update
    void Start()
    {
        spawnPos = new Vector2(this.transform.position.x, this.transform.position.y);
        StartCoroutine(spawnEnemy());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator spawnEnemy()
    {
        while (true)
        {
            
            GameObject newEnemy = Instantiate(enemy[Random.Range(0, enemy.Length)]);
            newEnemy.transform.position = spawnPos;

            if (newEnemy.name == "Wall(Clone)")
            {
                newEnemy.transform.localScale = new Vector2(newEnemy.transform.localScale.x, (newEnemy.transform.localScale.y + (0.020f * Singleton.Instance.level / 2)));
                newEnemy.transform.position = new Vector2(newEnemy.transform.position.x, (newEnemy.transform.position.y + (0.045f * Singleton.Instance.level / 2)));
                //newEnemy.transform.localScale = new Vector2(newEnemy.transform.localScale.x, (newEnemy.transform.localScale.y + (0.020f * level)));
                //newEnemy.transform.position = new Vector2(newEnemy.transform.position.x, (newEnemy.transform.position.y + (0.044f * level)));
            }

            // tiempo
            int tiempoEspera;
            if (Singleton.Instance.level > 25)
                tiempoEspera = 1;
            else
                tiempoEspera = Random.Range(6-(Singleton.Instance.level / 5), 12- (Singleton.Instance.level / 4));
            yield return new WaitForSeconds(tiempoEspera);
        }
    }

}
