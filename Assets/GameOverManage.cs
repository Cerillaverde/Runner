using System.Collections;
using System.Collections.Generic;
using System.Threading;
using Newtonsoft.Json;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverManage : MonoBehaviour
{
    byte contador = 1;
    int index = 0;
    bool waiting = false;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Incremento());
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && waiting)
        {
            SceneManager.LoadScene("MenuScene");
        }
    }

    

    IEnumerator Incremento()
    {
        while (true)
        {
            if (index > 2)
            {
                break;
            }
            else
            {
                yield return new WaitForSeconds(0.01f);

                this.transform.GetChild(index).gameObject.GetComponent<TMPro.TextMeshProUGUI>().color = new Color32(0, 0, 0, contador);

                contador++;

                if (contador >= 255)
                {
                    contador = 0;
                    index++;
                }
            }
        }
        StartCoroutine(esc());
    }

    IEnumerator esc()
    {
        waiting = true;
        while (true)
        {
            this.transform.GetChild(3).gameObject.GetComponent<TMPro.TextMeshProUGUI>().color = new Color32(255, 255, 255, 0);
            yield return new WaitForSeconds(1);
            this.transform.GetChild(3).gameObject.GetComponent<TMPro.TextMeshProUGUI>().color = new Color32(255, 255, 255, 255);
            yield return new WaitForSeconds(1);
        }
    }



}
