using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class FrogController : MonoBehaviour
{

    bool jump = false;
    public Sprite[] stateFrog;


    void Start()
    {
        StartCoroutine(cooldownForJump());
    }

    // Update is called once per frame
    void Update()
    {
        frogJump();
        outOfRange();
    }

    private void outOfRange()
    {
        if (this.transform.position.x < -10)
        {
            Destroy(this.gameObject);
        }
    }

    private void frogJump()
    {
        if (jump)
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(-4, this.GetComponent<Rigidbody2D>().velocity.y);
            this.GetComponent<SpriteRenderer>().sprite = stateFrog[1];
        }
        else
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(-2, this.GetComponent<Rigidbody2D>().velocity.y);
            this.GetComponent<SpriteRenderer>().sprite = stateFrog[0];
        }
    }

    IEnumerator cooldownForJump()
    {
        while (true)
        {
            if (!jump)
            {
                this.GetComponent<Rigidbody2D>().AddForce(new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, 500));
                jump = true;
            }
            yield return new WaitForSeconds(5f-(Singleton.Instance.level/10));
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "Plataforma")
        {
            jump = false;
        }
        if(collision.transform.tag == "Player")
        {
            Destroy(this.gameObject);
        }
        if (collision.gameObject.tag == "Limit")
        {
            Physics2D.IgnoreCollision(collision.gameObject.GetComponent<Collider2D>(), GetComponent<Collider2D>());
        }

    }

}
