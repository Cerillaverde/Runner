using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LifeCounter : MonoBehaviour
{
    int startingLives = 3;

    private int currentLives;

    public GameObject player;

    void Start()
    {
        currentLives = startingLives;
        player.GetComponent<PlayerController>().OnLoseLife += LoseLife;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void LoseLife()
    {
        if (currentLives >= 0)
        {
            Transform vidaTransform = this.transform.GetChild(currentLives-1);
            vidaTransform.gameObject.SetActive(false);
            currentLives--;
        }
        if (currentLives == 0)
        {
            SceneManager.LoadScene("GameOver");
        }
    }

}
