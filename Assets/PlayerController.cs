using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    bool jump = false;
    int jumpCount = 0;
    public GameObject vidasObject;

    public delegate void LoseLifeHandler();
    public event LoseLifeHandler OnLoseLife;


    // /Start is called before the first frame update
    void Start()
    {
        StartCoroutine(cooldownJump());
    }

    // Update is called once per frame
    void Update()
    {
        moveCharacter();
    }

    private void moveCharacter()
    {
        // Move on right or left.
        if (Input.GetKey(KeyCode.A))
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(-8, this.GetComponent<Rigidbody2D>().velocity.y);
        }
        else if (Input.GetKey(KeyCode.D))
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(3, this.GetComponent<Rigidbody2D>().velocity.y);
        }
        else
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(-2, this.GetComponent<Rigidbody2D>().velocity.y);
        }

        // Jump
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (!jump && jumpCount == 0)
            {
                this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 500));
                jumpCount++;
            }
            else
            {
                if (jumpCount < 2)
                {
                    this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, 0);
                    this.GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 500));
                    jumpCount++;

                }
            }
        }

        // bend
        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            this.transform.localScale = new Vector2(this.transform.localScale.x, this.transform.localScale.y / 2f);
        }
        else if (Input.GetKeyUp(KeyCode.LeftControl))
        {
            this.transform.localScale = new Vector2(this.transform.localScale.x, this.transform.localScale.y * 2f);
        }

    }


    private void OnCollisionEnter2D(Collision2D collision)
    {

        if (collision.transform.tag == "Plataforma")
        {
            jump = false;
            jumpCount = 0;
        }
        if (collision.transform.tag == "enemy")
        {
            ColisionEnemigo();
        }
    }

    public void ColisionEnemigo()
    {
        OnLoseLife?.Invoke();
    }

    IEnumerator cooldownJump()
    {
        if (jump)
        {
            yield return new WaitForSeconds(0.1f);
            jump = true;
        }
    }
}