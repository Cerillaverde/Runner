using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class shootScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Accelerate());
    }

    // Update is called once per frame
    void Update()
    {
        this.GetComponent<Rigidbody2D>().velocity = new Vector2 (-1f, this.GetComponent<Rigidbody2D>().velocity.y);
        if (this.transform.position.y <= -5)
        {
            Destroy(this.gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            collision.gameObject.GetComponent<PlayerController>().ColisionEnemigo();
            Destroy(this.gameObject);
        }
    }

    IEnumerator Accelerate()
    {
        this.GetComponent<Rigidbody2D>().gravityScale += 0.1f;
        yield return new WaitForSeconds(0.1f+(Singleton.Instance.level/50));
    }
}
