using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrakeController : MonoBehaviour
{

    bool waiting=false;

    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(-4, 0);
    }

    // Update is called once per frame
    void Update()
    {
        if (this.transform.position.x <= -10)
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
            StartCoroutine(Move());
        }
    }

    IEnumerator Move()
    {
        if (!waiting)
        {
            waiting = true;
            yield return new WaitForSeconds(30-Singleton.Instance.level);
            this.transform.position = new Vector2(22, this.transform.position.y);
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(-4, 0);
            waiting = false;
        }
    }
}
